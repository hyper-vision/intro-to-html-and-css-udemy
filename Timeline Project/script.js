// Grabbing all li items from timeline ul
const items = document.querySelectorAll("#timeline li");

// Checks to see if given item is within viewport
const isInViewport = el => {
  const rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <=
      (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

// Loops through li's and checks if item in viewport
// If in viewport then add class of show
const run = () =>
  items.forEach(item => {
    if (isInViewport(item)) {
      item.classList.add("show");
    }
  });

// Events (fires function run at load, window resize and scroll events)
window.addEventListener("load", run);
window.addEventListener("resize", run);
window.addEventListener("scroll", run);
